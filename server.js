/*--------------------------------------------------------------------------------
for docker
const ronin = require( 'ronin-server' )
const mocks = require( 'ronin-mocks' )
const database = require( 'ronin-database' )
const server = ronin.server()

database.connect( process.env.CONNECTIONSTRING )
// server.use( '/', mocks.server( server.Router(), false, false ) )
server.use( '/foo', (req, res) => {
    return res.json({ "foo": "bar" })
  })
server.start()
--------------------------------------------------------------------------------*/

const express = require('express');

const app = express();
const userRouter = require('./routes/users');

app.set('view engine', 'ejs');

app.use(express.urlencoded({ extended: true }));
app.use(express.static('public'));
app.use(express.json());
app.use('/users', userRouter);

app.listen(8080);

module.exports = app;
