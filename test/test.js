const assert = require('assert');
/* eslint-disable no-undef */
describe('Array', () => {
  describe('#indexOf()', () => {
    it('should return -1 when the value is not present', () => {
      assert.equal([1, 2, 3].indexOf(4), -1);
    });
  });
});

/*
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');

const should = chai.should();

chai.use(chaiHttp);

describe('/GET users', () => {
  it('it should GET the text', (done) => {
    chai.request(server)
      .get('/')
      .end((err, res) => {
        // console.log(res);
        res.should.have.status(200);
        res.body.should.be.a('text/html');
        res.body.length.should.be.eql(0);
        done();
      });
  });
});
*/
