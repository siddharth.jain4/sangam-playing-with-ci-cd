# syntax=docker/dockerfile:1

#creating a base image
FROM node:12.14.1 

# environment in which an application is running 
ENV NODE_ENV=production

#optional
LABEL siddharth, tech@sangam.com

# This instructs Docker to use this path as the default location for all subsequent commands. 
# This way we do not have to type out full file paths but can use relative paths based 
# on the working directory.
WORKDIR /usr/src/app


# COPY command takes two parameters: src and dest. 
# The first parameter src tells Docker what file(s) you would like to copy into the image. 
# The second parameter dest tells Docker where you want that file(s) to be copied to.
# COPY ["<src>", "<dest>"]
#Before we can run npm install, we need to get our package.json and package-lock.json files into our images.
COPY ["package.json", "package-lock.json*", "./"]
# shorthand used by us:-- COPY package*.json ./


# Once we have our files inside the image, we can use the RUN command to execute the 
# command npm install. This works exactly the same as if we were running npm install locally 
# on our machine, but this time these Node modules will be installed into the 
# node_modules directory inside our image.
RUN npm install

# At this point, we have an image that is based on node version 12.14.1 
# and we have installed our dependencies. The next thing we need to do is to 
# add our source code into the image. We’ll use the COPY command just like we 
# did with our package.json files above.
COPY . .

EXPOSE 8080

# Now, all we have to do is to tell Docker what command we want to run when our 
# image is run inside of a container. We do this with the CMD command.
CMD [ "node", "server.js" ]

