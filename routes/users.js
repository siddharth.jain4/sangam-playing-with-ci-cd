const express = require('express');

const router = express.Router();

const users = [
  {
    name: 'siddharth',
  },
  {
    name: 'shreyans',
  },

];
router.get('/', (req, res) => {
  res.send('users');
});

router.get('/new', (req, res) => {
  // res.send('New User Form')
  res.render('users/new');
});

router.post('/', (req, res) => {
  const isValid = true;
  if (isValid) {
    users.push({ name: req.body.firstName });
    res.redirect(`/users/${users.length - 1}`);
  } else {
    console.log('Error new');
    res.render('users/new', { firstName: req.body.firstName });
  }
  // res.send('created user ' + req.body.firstName)
});

router
  .route('/:id')
  .get((req, res) => {
    res.send(`${req.params.id}.  user  ${req.user.name}`);
  })
  .put((req, res) => {
    res.send(`user ${req.params.id}updated`);
  })
  .delete((req, res) => {
    res.send(`user ${req.params.id}deleted`);
  });

router.param('id', (req, res, next, id) => {
  req.user = users[id] || { name: 'def' };
  next();
});

module.exports = router;
